function carregar() {
    var msg = window.document.getElementById('msg')
    var img = window.document.getElementById('imagem')
    var data = new Date()
    var hora = data.getHours()

    msg.innerHTML = `Agora sao ${hora} horas`

    if (hora >= 0 && hora <= 12) {
        msg.innerHTML = 'Bom dia <br/>'
        msg.innerHTML += `Agora sao ${hora} horas`
        img.src = 'manha.png'
        document.body.style.background = '#f8f0a3'
    } else if (hora < 18) {
        msg.innerHTML = 'Boa tarde <br/>'
        img.src = 'tarde.png'
        msg.innerHTML += `Agora sao ${hora} horas`
        document.body.style.background = '#71573c'
    } else {
        msg.innerHTML = 'Boa noite <br/>'
        msg.innerHTML += `Agora sao ${hora} horas`
        img.src = 'noite.png'
        document.body.style.background = '#000b17'
    }
}