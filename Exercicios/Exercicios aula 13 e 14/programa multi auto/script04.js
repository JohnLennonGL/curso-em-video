 function resultado() {
     var num = window.document.getElementById('txtn')
     var tab = window.document.getElementById('seltab')


     if (num.value.length == 0) {
         alert('Digite um numero')
     } else {
         var n = Number(num.value)
         tab.innerHTML = ''
         for (c = 1; c <= 10; c++) {
             var item = document.createElement('option')
             item.text = `${n} x ${c} = ${c*n}`
             tab.appendChild(item)
         }
     }

 }