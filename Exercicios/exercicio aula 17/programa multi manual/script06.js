 var num = window.document.getElementById('fnum')
 var lista = window.document.getElementById('lista')
 var res = document.getElementById('res')
 var valores = []

 function isNumero(n) {
     if (Number(n) >= 1 && Number(n) <= 100) {
         return true

     } else {
         return false
     }
 }

 function islista(n, l) {
     if (l.indexOf(Number(n)) != -1) {
         return true
     } else {
         return false
     }
 }

 function adicionar() {
     if (isNumero(num.value) && !islista(num.value, valores)) {
         valores.push(Number(num.value))
         var item = document.createElement('option')
         item.text = ` Numero ${num.value} Adicionado`
         lista.appendChild(item)
         res.innerHTML = ''

     } else {
         alert('erro')
     }
     num.value = ''
     num.focus()
 }

 function finalizar() {
     var tot = valores.length
     var maior = valores[0]
     var menor = valores[0]
     var soma = 0
     var media = 0
     for (var pos in valores) {
         soma += valores[pos]
         if (valores[pos] > maior)
             maior = valores[pos]
         if (valores[pos] < menor)
             menor = valores[pos]
     }

     media = soma / tot
     res.innerHTML = ''
     res.innerHTML += `Ao todo temos ${tot} numeros registrados <br/>`
     res.innerHTML += `O maior numero é o ${maior}<br/>`
     res.innerHTML += `O menor numero é o ${menor}<br/>`
     res.innerHTML += `A soma de todos os numeros é ${soma}<br/>`
     res.innerHTML += `A media de todos os valores é ${media}<br/>`

 }